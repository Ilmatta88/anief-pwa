import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the SearchPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'search',
})
export class SearchPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
 //   transform(items: any[], terms: string): any[] {
 //   if(!items) return [];
 //   if(!terms) return items;
 //   terms = terms.toLowerCase();
 //   return items.filter( it => {
 //     return it.item.toLowerCase().includes(terms); // only filter country name
 //   });
 // }

 transform(items: any, term: any): any {
     if (term === undefined) return items;

     return items.filter(function(Item) {
         return Item.item.toLowerCase().includes(term.toLowerCase());
     })
 }


}
