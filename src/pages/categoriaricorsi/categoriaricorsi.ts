import { Component } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import 'rxjs/add/operator/map';
import { SedeSingolaPage } from '../sedesingola/sedesingola';
import { SingoloRicorsoPage} from '../singoloricorso/singoloricorso';


@Component({
  selector: 'page-categoriaricorsi',
  templateUrl: 'categoriaricorsi.html'
})
export class CategoriaRicorsiPage {
item:any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,

  ) { this.item = this.navParams.get('item'); }


  ionViewWillEnter() {
      
  }


    ionViewDidLoad() {

  }
  goToPaginaSingoloRicorso(v){
  this.navCtrl.push(SingoloRicorsoPage, {v: v});
  }


}
