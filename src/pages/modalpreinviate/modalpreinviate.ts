import { Component, OnInit } from '@angular/core';
import { NavController, NavParams,ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import {PreadesioniinviatePage } from '../preadesioniinviate/preadesioniinviate';


@Component({
  selector: 'page-modalpreinviate',
  templateUrl: 'modalpreinviate.html'
})

export class ModalpreinviatePage implements OnInit {
  item = {};

  constructor(public nav: NavController, public navParams: NavParams,private storage: Storage,private http: HttpClient, private modalController: ModalController
) {
  this.item = this.navParams.get('item');
  }

  ngOnInit() {
  }



  goback(){
    this.nav.pop();
 }





}
