import { Component } from '@angular/core';
import { NavController, NavParams,   } from 'ionic-angular';
import { LoginPage} from '../login/login';
import { HomePage} from '../home/home';
import { ContattiPage} from '../contatti/contatti';
import { RicorsiPage} from '../ricorsi/ricorsi';
import { Storage } from '@ionic/storage';
import { IntroduzionePage } from '../introduzione/introduzione';

@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})
export class WelcomePage {
  showBtn: boolean = false;
  deferredPrompt;
  uuid: string = "";
  hide:boolean = true;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    ) {

  }



      // forceReload(refresher){
      //
      // }

  ionViewWillEnter(){
    window.addEventListener('beforeinstallprompt', (e) => {
      e.preventDefault();
      this.deferredPrompt = e;
      this.showBtn = true;
    });
    window.addEventListener('appinstalled', (event) => {
     alert('Installata');
    });
    if (window.matchMedia('(display-mode: standalone)').matches) {

    }
  }

  ionViewDidLoad() {
    this.storage.get('intro-done').then(done => {
    if (!done) {
     this.storage.set('intro-done', true);
     this.navCtrl.setRoot(IntroduzionePage);
   }
  });

}

  add_to_home(e){
    debugger
    // hide our user interface that shows our button
    // Show the prompt
    this.deferredPrompt.prompt();
    // Wait for the user to respond to the prompt
    this.deferredPrompt.userChoice
      .then((choiceResult) => {
        if (choiceResult.outcome === 'accepted') {
          alert('Applicazione Installata');
        } else {
        }
        this.deferredPrompt = null;
      });
  };






  gotoLoginpage(){
    this.navCtrl.push(LoginPage);
  }
  gotoHomePage(){
    this.navCtrl.push(HomePage);
  }
  gotoContattiPage(){
    this.navCtrl.push(ContattiPage);
  }
  gotoRicorsiPage(){
    this.navCtrl.push(RicorsiPage);
  }





} //fine classe pagina
