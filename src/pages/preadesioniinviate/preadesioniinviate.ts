import { Component } from '@angular/core';
import { NavController, NavParams,ModalController  } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {LoginPage} from '../login/login';
import { ModalpreinviatePage} from '../modalpreinviate/modalpreinviate';

@Component({
  selector: 'page-preadesioniinviate',
  templateUrl: 'preadesioniinviate.html'
})

export class PreadesioniinviatePage {
  jsonData = {};
  userdata:any;
  storageItems:any;
  requestData = {};
  data:any;

  constructor(public nav: NavController, public modalCtrl: ModalController , public navParams: NavParams,private storage: Storage
) {

  }

  ionViewDidLoad() {
    this.storage.get('ricorsiutenti').then((val) => {
      this.storageItems = val;
      this.storageItems.user = val.userData;
      this.data = this.storageItems.user;
      });
}

  public logout() {
    this.storage.set('isLogin', false);
    this.storage.set('userdata', null);
    this.nav.setRoot(LoginPage);
  }

  public gotoLogin(){
    this.nav.setRoot(LoginPage);
  }



  public doalert(item){
    let profileModal = this.modalCtrl.create(ModalpreinviatePage, { item: item });
        profileModal.present();

  }





}
