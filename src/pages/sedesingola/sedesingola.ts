import { Component } from '@angular/core';
import { NavController, NavParams,   } from 'ionic-angular';
import 'rxjs/add/operator/map';


@Component({
  selector: 'page-sedesingola',
  templateUrl: 'sedesingola.html'
})

export class SedeSingolaPage {
  v:any = [];


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
    ) {
      this.v = this.navParams.get('v');


}

  ionViewWillEnter() {
    console.log(this.v);
  }


}
