import { Component } from '@angular/core';
import { NavController, NavParams,   } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { SediProvider} from '../../providers/sedi/sedi';
import { SedeProvinciaPage } from '../sedeprovincia/sedeprovincia';


@Component({
  selector: 'page-sedinazionali',
  templateUrl: 'sedinazionali.html'
})

export class SedinazionaliPage {
  arrayNews: Array<any> = [];
  errorMessage: string;
  item:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public SediProvider: SediProvider,) {

}

  ionViewWillEnter() {
this.getTutteLeSedi();
  }

  getTutteLeSedi() {
  this.SediProvider.getSedi()
     .subscribe(
        data => this.arrayNews = data.jsonData,//here
        error =>  this.errorMessage = <any>error);
    }

    goToPaginaProvincia(item){
        this.navCtrl.push(SedeProvinciaPage, {item: item});

    }

}
