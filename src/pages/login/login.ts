import { Component,   } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading,  AlertController} from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { HttpClient } from '@angular/common/http';
import { AccountPage} from '../account/account';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


export interface IjsonData {
   state: number;
   accesstoken: string;
   id: string;
}

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
    loading: Loading;
    requestData = {};
    data: any;
    jsonData: any;
    userdata: any;
    email: string;
    success: Array<any>;
    state: number;

  constructor(
    private navParm: NavParams,
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private http: HttpClient,
    private loadingCtrl: LoadingController,
    private storage: Storage,
  ) { }

ionViewWillEnter(){
  this.presentLoadingDefault();
  this.storage.get('isLogin').then(state => {
  if (state == true ) {
     this.navCtrl.setRoot(AccountPage);
 }
});
}

  ionViewDidLoad() {

}





presentLoadingDefault() {
let loading = this.loadingCtrl.create({
  spinner: 'bubbles',
  content: 'Caricamento Dati...'
  });

loading.present();

setTimeout(() => {
loading.dismiss();
}, 1000);
}




  public showLoading() {
      this.loading = this.loadingCtrl.create({
        content: 'Attendere....',
        dismissOnPageChange: true
      });
      this.loading.present();
    }

  public showError(text) {
    let alert = this.alertCtrl.create({
      title: 'Dati di login non corretti',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }


  public login(){

   // console.log(this.requestData);
   var link = 'https://www.anief.org/webservices/index.php?fnc=login';
   var datapost = JSON.stringify({requestData: this.requestData });
   return this.http.post(link, datapost)
       .subscribe(
         (data:any) => {
           this.jsonData = data.jsonData;
           this.state = data.jsonData.state;
           if (this.state == 1 ) {
               this.showLoading();
               this.storage.get('isLogin').then(done => {
               if (!done) {
                this.storage.set('isLogin', true);
                this.storage.set('userdata', this.jsonData);
                this.gotoAccount();
              }
             });
           }
         else  {
           let alert = this.alertCtrl.create({
             title: 'Dati di login non corretti',
             subTitle: '',
             buttons: ['OK']
           });
           alert.present();
       }
       },
       error => {
           this.showError(error);
         });

  }




  public gotoregister(){
    this.navCtrl.push(RegisterPage);
  }

  public gotoAccount(){
    this.navCtrl.setRoot(AccountPage, {jsonData: this.jsonData});
  }





}
