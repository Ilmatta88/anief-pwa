import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {LoginPage} from '../login/login';
import { HttpClient } from '@angular/common/http';
import {SingolanotificaPage } from '../singolanotifica/singolanotifica';



@Component({
  selector: 'page-notifiche',
  templateUrl: 'notifiche.html'
})

export class NotifichePage {
  storageItems:any;
  jsonData = {};
  userdata:any;
  requestData = {};
  data:any;
  storageItems2:any;

  constructor(public nav: NavController, public navParams: NavParams,private storage: Storage,private http: HttpClient
) {

  }

  ionViewDidLoad() {
    this.trovanotifiche();
    this.parseid();
}

  public logout() {
    this.storage.set('isLogin', false);
    this.storage.set('userdata', null);
    this.nav.setRoot(LoginPage);
  }

  public gotoLogin(){
    this.nav.setRoot(LoginPage);
  }




  public trovanotifiche(){
    this.storage.get('userdata').then((val) => {
      this.storageItems = val;
      this.storageItems.id = val.id;
      this.storageItems.token = val.accessToken;
      this.requestData = {'token': this.storageItems.token, 'id': this.storageItems.id };


     var link = 'https://anief.org/webservices/index.php?fnc=notifiche';
     var datapost = JSON.stringify({requestData: this.requestData });
      this.http.post(link, datapost)
       .subscribe(
               (data:any) => {
                 this.jsonData = data.jsonData;
                 this.storage.set('notificheutenti', this.jsonData);
                  },
              error => {
                    console.log('errore');
                    });

  });
}


public parseid(){

  this.storage.get('notificheutenti').then((value) => {
    this.storageItems2 = value;
    this.storageItems2.notifiche = value.userData;
    this.data = this.storageItems2.notifiche;
    console.log(this.data);
  });

}

public gotosingolanotifica(item){
  this.nav.push(SingolanotificaPage, {item:item})
}




}
