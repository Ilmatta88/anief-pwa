import { Component } from '@angular/core';
import { NavController, NavParams,   } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { SedeSingolaPage } from '../sedesingola/sedesingola';


@Component({
  selector: 'page-sedeprovincia',
  templateUrl: 'sedeprovincia.html'
})

export class SedeProvinciaPage {
  item:any = [];


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
    ) {
      this.item = this.navParams.get('item');


}

  ionViewWillEnter() {
    console.log(this.item);
  }

  goToPaginaSingola(v){
      this.navCtrl.push(SedeSingolaPage, {v: v});

  }


}
