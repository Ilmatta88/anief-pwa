import { Component } from '@angular/core';
import { NavController,    } from 'ionic-angular';
import { NewsProvider} from '../../providers/news/news';
import { SingolanewsPage} from '../singolanews/singolanews';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  arrayNews: Array<any> = [];
  errorMessage: string;
  item:any;
  itemKey: 'my-news-group';



  constructor(
    public navCtrl: NavController,
    public NewsProvider: NewsProvider,
    public storage: Storage
  ) {

  }


      // forceReload(refresher){
      //
      // }

  ionViewWillEnter(){
    this.getTutteLeNews();

  }









//carico tutte le news
    getTutteLeNews() {
    this.NewsProvider.getNews()
       .subscribe(
          data => this.arrayNews = data.jsonData,//here
          error =>  this.errorMessage = <any>error);

                  }



//push della singola news
goTosingolaNews(item){
    this.navCtrl.push(SingolanewsPage, {item: item});
  }

  gotoLoginpage(){
    this.navCtrl.push(LoginPage);
  }





} //fine classe pagina
