import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ContattisegreteriaPage} from '../contattisegreteria/contattisegreteria';
import { ContattiamministrazionePage} from '../contattiamministrazione/contattiamministrazione';
import { SedinazionaliPage } from '../sedinazionali/sedinazionali';


@Component({
  selector: 'page-contatti',
  templateUrl: 'contatti.html'
})

export class ContattiPage {


  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  ionViewDidLoad() {

  }

  gotoSegreteriaNazionale(){
      this.navCtrl.push(ContattisegreteriaPage);
   }
   gotoAmministrazione(){
       this.navCtrl.push(ContattiamministrazionePage);
    }
    goToSedinazionali(){
      this.navCtrl.push(SedinazionaliPage);
    }


}
