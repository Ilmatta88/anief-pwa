import { Component } from '@angular/core';
import { NavController, NavParams, } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { RicorsiProvider} from '../../providers/ricorsi/ricorsi';
import { CategoriaRicorsiPage } from '../categoriaricorsi/categoriaricorsi';
import { Storage} from '@ionic/storage';


@Component({
  selector: 'page-ricorsi',
  templateUrl: 'ricorsi.html'
})
export class RicorsiPage {

  arrayNews: Array<any> = [];
  errorMessage: string;
  item:any;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public RicorsiProvider: RicorsiProvider,
    private storage: Storage
  ) { }


  ionViewWillEnter() {
this.getTuttiiRicorsi();
this.test();

  }


    ionViewDidLoad() {


  }

  getTuttiiRicorsi() {
  this.RicorsiProvider.getRicorsi()
     .subscribe(

        data => this.arrayNews = data.jsonData.items,//here
        error =>  this.errorMessage = <any>error);
    }

    test(){
this.RicorsiProvider.getRemoteData();


    }
    goToPaginaCategoriaRicorsi(item){
      this.navCtrl.push(CategoriaRicorsiPage, {item: item});
    }

    salvaricorsi(){
      this.storage.set('tuttiricorsi',this.arrayNews);
    }







}
