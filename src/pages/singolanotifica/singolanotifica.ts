import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {LoginPage} from '../login/login';
import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'page-singolanotifica',
  templateUrl: 'singolanotifica.html'
})

export class SingolanotificaPage {
  storageItems:any;
  jsonData = {};
  userdata:any;
  requestData = {};
  v:any;

  constructor(public nav: NavController, public navParams: NavParams,private storage: Storage,private http: HttpClient
) {
  this.v = this.navParams.get('item');



  }

  ionViewDidLoad() {

}

  public logout() {
    this.storage.set('isLogin', false);
    this.storage.set('userdata', null);
    this.nav.setRoot(LoginPage);
  }

  public gotoLogin(){
    this.nav.setRoot(LoginPage);
  }










}
