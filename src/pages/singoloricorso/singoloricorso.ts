import { Component } from '@angular/core';
import { NavController, NavParams, AlertController   } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { Storage} from '@ionic/storage';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-singoloricorso',
  templateUrl: 'singoloricorso.html'
})
export class SingoloRicorsoPage {
  v:any = [];
  jsonData = {};
  userdata:any;
  storageItems:any;
  requestData = {};
  data:any;
idricorso:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private http: HttpClient,
    public alertCtrl: AlertController
    ) {
      this.v = this.navParams.get('v');
      this.idricorso = this.v.id;


  }

  ionViewWillEnter() {

  }


  preferiti(){

    this.storage.get('ricorso').then((val) => {
        console.log(val + " = previous value");
        val.push(this.v);
        this.storage.set('ricorso', val);
      });
    this.storage.set('ricorso', this.v);
    }


  addpreferiti(){
    this.idricorso = this.v.id;
    /*Prendo i dati dell'utente loggato*/
    this.storage.get('userdata').then((val) => {
      this.storageItems = val;
      this.storageItems.id = val.id;
      this.storageItems.token = val.accessToken;
      this.requestData = {'action': 'add', 'id_ricorso': this.idricorso,'token': this.storageItems.token, 'id': this.storageItems.id };
     var link = 'https://www.anief.org/webservices/index.php?fnc=wishlist';
     var datapost = JSON.stringify({requestData: this.requestData });
      this.http.post(link, datapost)
       .subscribe(
               (data:any) => {
                 this.jsonData = data.jsonData;

                
                },
              error => {
                    console.log('errore');
                    });
  });




  }


    // public presentAlert() {
    //   let alert = this.alertCtrl.create({
    //     title: 'Hai aggiunto questo ricorso tra le tue preadesioni',
    //     subTitle: 'Controlla lo status del tuo ricorso dalla pagina dedicata',
    //     buttons: ['OK']
    //   });
    //   alert.present();
    // }

    public presentAlert() {
        let alert = this.alertCtrl.create({
      title: 'Vuoi aggiungere questo ricorso tra le tue preadesioni?',
      message: 'Potrai controllare lo status del tuo ricorso dalla pagina dedicata',
      buttons: [
        {
          text: 'Annulla',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'Si',
          handler: () => {
            this.addpreferiti();
          }
        }
      ]
    });
    alert.present();
  }



}
