import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {LoginPage} from '../login/login';
import { HttpClient } from '@angular/common/http';
import {PreadesioniinviatePage } from '../preadesioniinviate/preadesioniinviate';
import { PreadesioniPage } from '../preadesioni/preadesioni';
import { ContenziosoPage } from '../contenzioso/contenzioso';
import { NotifichePage } from '../notifiche/notifiche';


@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})

export class AccountPage {
  jsonData = {};
  userdata:any;
  storageItems:any;
  requestData = {};

  constructor(public nav: NavController, public navParams: NavParams,private storage: Storage,private http: HttpClient
) {

  }

  ionViewDidLoad() {

    this.trovaricorsi();
    this.trovanotifiche();
    this.trovacontenziosi();
    this.getlistpreadesioni();
}

  public logout() {
    this.storage.set('isLogin', false);
    this.storage.set('userdata', null);
    this.nav.setRoot(LoginPage);
  }

  public gotoLogin(){
    this.nav.setRoot(LoginPage);
  }

  public gotoPreades(){
    this.nav.push(PreadesioniinviatePage);
  }

  public gotoPreadesioniPage(){
    this.nav.push(PreadesioniPage);
  }
  public gotoContenziosoPage(){
    this.nav.push(ContenziosoPage);
  }

  public gotonotifichepage(){
    this.nav.push(NotifichePage);
  }


  public trovaricorsi(){
    this.storage.get('userdata').then((val) => {
      this.storageItems = val;
      this.storageItems.id = val.id;
      this.storageItems.token = val.accessToken;
      this.requestData = {'token': this.storageItems.token, 'id': this.storageItems.id };


     var link = 'https://www.anief.org/webservices/index.php?fnc=preadesioni';
     var datapost = JSON.stringify({requestData: this.requestData });
      this.http.post(link, datapost)
       .subscribe(
               (data:any) => {
                 this.jsonData = data.jsonData;
                 this.storage.set('ricorsiutenti', this.jsonData);
                  },
              error => {
                    console.log('errore');
                    });

  });
}


public trovacontenziosi(){
  this.storage.get('userdata').then((val) => {
    this.storageItems = val;
    this.storageItems.id = val.id;
    this.storageItems.token = val.accessToken;
    this.requestData = {'token': this.storageItems.token, 'id': this.storageItems.id };


   var link = 'https://www.anief.org/webservices/index.php?fnc=contenzioso';
   var datapost = JSON.stringify({requestData: this.requestData });
    this.http.post(link, datapost)
     .subscribe(
             (data:any) => {
               this.jsonData = data.jsonData;
               this.storage.set('contenziosiutenti', this.jsonData);
                },
            error => {
                  console.log('errore');
                  });

});
}


public trovanotifiche(){
  this.storage.get('userdata').then((val) => {
    this.storageItems = val;
    this.storageItems.id = val.id;
    this.storageItems.token = val.accessToken;
    this.requestData = {'token': this.storageItems.token, 'id': this.storageItems.id };


   var link = 'https://anief.org/webservices/index.php?fnc=notifiche';
   var datapost = JSON.stringify({requestData: this.requestData });
    this.http.post(link, datapost)
     .subscribe(
             (data:any) => {
               this.jsonData = data.jsonData;
               this.storage.set('notificheutenti', this.jsonData);
                },
            error => {
                  console.log('errore');
                  });

});
}

public getlistpreadesioni(){
  /*Prendo i dati dell'utente loggato*/
  this.storage.get('userdata').then((val) => {
    this.storageItems = val;
    this.storageItems.id = val.id;
    this.storageItems.token = val.accessToken;
    this.requestData = {'action': 'getlistext', 'token': this.storageItems.token, 'id': this.storageItems.id };
   var link = 'https://www.anief.org/webservices/index.php?fnc=wishlist';
   var datapost = JSON.stringify({requestData: this.requestData });
    this.http.post(link, datapost)
     .subscribe(
             (data:any) => {
               this.jsonData = data.jsonData;
               this.storage.set('idpreadesionidacompletare', this.jsonData);
                },
            error => {
                  console.log('errore');
                  });
                });
              }





}
