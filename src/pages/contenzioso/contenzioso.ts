import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {LoginPage} from '../login/login';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'page-contenzioso',
  templateUrl: 'contenzioso.html'
})

export class ContenziosoPage {
  storageItems:any;
  requestData:any;
  jsonData:any;
  storageObj:any;
  data:any;
  ricorsi:any;

  constructor(public nav: NavController, public navParams: NavParams,private storage: Storage,private http: HttpClient) {
  }

  ionViewDidLoad() {

    this.trovacontenziosi();
    this.storage.get('contenziosiutenti').then((val) => {
      this.storageObj = val;
      this.storageObj.ricorsi = val.ricorsi;
      this.data = this.storageObj.ricorsi;
      });


  }

  public trovacontenziosi(){
    this.storage.get('userdata').then((val) => {
      this.storageItems = val;
      this.storageItems.id = val.id;
      this.storageItems.token = val.accessToken;
      this.requestData = {'token': this.storageItems.token, 'id': this.storageItems.id };


     var link = 'https://www.anief.org/webservices/index.php?fnc=contenzioso';
     var datapost = JSON.stringify({requestData: this.requestData });
      this.http.post(link, datapost)
       .subscribe(
               (data:any) => {
                 this.jsonData = data.jsonData;
                 this.storage.set('contenziosiutenti', this.jsonData);
                  },
              error => {
                    console.log('errore');
                    });

  });
}


}
