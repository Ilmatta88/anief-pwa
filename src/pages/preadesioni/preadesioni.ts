import { Component } from '@angular/core';
import { NavController, NavParams, AlertController} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {LoginPage} from '../login/login';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'page-preadesioni',
  templateUrl: 'preadesioni.html'
})

export class PreadesioniPage {
  storageItems:any;
  jsonData = {};
  userdata:any;
  requestData = {};
  storageItems2:any;
  listaiditems:any;
data:any;
idricorso:number;



  constructor(public nav: NavController,
    public alertCtrl: AlertController,
     public navParams: NavParams,
     private storage: Storage,
     private http: HttpClient) {
  }

  ionViewDidLoad() {
    this.getlistpreadesioni();
    this.parseid();
  }





  getlistpreadesioni(){
    /*Prendo i dati dell'utente loggato*/
    this.storage.get('userdata').then((val) => {
      this.storageItems = val;
      this.storageItems.id = val.id;
      this.storageItems.token = val.accessToken;
      this.requestData = {'action': 'getlistext', 'token': this.storageItems.token, 'id': this.storageItems.id };
     var link = 'https://www.anief.org/webservices/index.php?fnc=wishlist';
     var datapost = JSON.stringify({requestData: this.requestData });
      this.http.post(link, datapost)
       .subscribe(
               (data:any) => {
                 this.jsonData = data.jsonData;
                 this.storage.set('idpreadesionidacompletare', this.jsonData);
                  },
              error => {
                    console.log('errore');
                    });
                  });
                }

  public removepreadesioni(item){
        this.storage.get('userdata').then((val) => {
        this.storageItems = val;
        this.storageItems.id = val.id;
        this.storageItems.token = val.accessToken;
        this.idricorso =  item.id_ricorso;
        this.requestData = {'action': 'remove', 'id_ricorso': this.idricorso, 'token': this.storageItems.token, 'id': this.storageItems.id, };
       var link = 'https://www.anief.org/webservices/index.php?fnc=wishlist';
       var datapost = JSON.stringify({requestData: this.requestData });
        this.http.post(link, datapost)
         .subscribe(
                 (data:any) => {
                   this.jsonData = data.jsonData;
                   let index = this.data.indexOf(item);

                   if(index > -1){
                     this.data.splice(index, 1);
                    }
                   },
                error => {
                      console.log('errore');
                      });
                    });
    }

    public parseid(){

      this.storage.get('idpreadesionidacompletare').then((value) => {
        this.storageItems2 = value;
        this.storageItems2.ricorso = value.response;
        this.data = this.storageItems2.ricorso;
        console.log(this.data);
      });

    }



  public confirmdelete(item) {
      let alert = this.alertCtrl.create({
    title: 'Conferma Rimozione',
    message: 'Sei sicuro di voler rimuovere questo ricorso?',
    buttons: [
      {
        text: 'Annulla',
        role: 'cancel',
        handler: () => {

        }
      },
      {
        text: 'Si',
        handler: () => {
          this.removepreadesioni(item);
        }
      }
    ]
  });
  alert.present();
}


  // public presentAlert(item) {
  //   let alert = this.alertCtrl.create({
  //     title: 'Sei sicuro di voler rimuovere questo ricorso?',
  //     subTitle: 'Questa operazione è irreversibile e non dovrai nuovamente registrarti se intendi proseguire nella tua azione',
  //     buttons: ['OK']
  //   });
  //   alert.present();
  //   this.removePost(item);
  //
  // }

}
