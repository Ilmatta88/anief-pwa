import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map';
/*
  Generated class for the SediProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class SediProvider {

  private apiUrl = 'https://www.anief.org/webservices/index.php?fnc=sedi';
  item : any;
  jsonData: any;



      constructor(public http: HttpClient) {
        }

        getSedi(): Observable<any> {
          return this.http.get(this.apiUrl).pipe(
            map(this.extractData),
            catchError(this.handleError)
          );
        }



        private extractData(res: Response) {
          let body = res;
          return body || {};
        }

        private handleError (error: Response | any) {
          let errMsg: string;
          if (error instanceof Response) {
            const err = error || '';
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
          } else {
            errMsg = error.message ? error.message : error.toString();
          }
          console.error(errMsg);
          return Observable.throw(errMsg);
        }

          getRemoteData(){
            this.http.get(this.apiUrl).map(res => res).subscribe(data => {
            
            });
          }

          getSediTest() {
            return new Promise(resolve => {
              this.http.get(this.apiUrl).subscribe(data => {
                resolve(data);},
                err => {
                  console.log(err);
                });
              });
            }



}
