import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ContattiPage } from '../pages/contatti/contatti';
import { ContattisegreteriaPage } from '../pages/contattisegreteria/contattisegreteria';
import { ContattiamministrazionePage } from '../pages/contattiamministrazione/contattiamministrazione';
import { SingolanewsPage } from '../pages/singolanews/singolanews';
import { SingolanotificaPage} from '../pages/singolanotifica/singolanotifica';
import { NotifichePage} from '../pages/notifiche/notifiche';

import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';


import { LoginPage } from '../pages/login/login'
import { RegisterPage } from '../pages/register/register';
import { WelcomePage } from '../pages/welcome/welcome';
import { RicorsiPage } from '../pages/ricorsi/ricorsi';
import { SedinazionaliPage } from '../pages/sedinazionali/sedinazionali';
import { SedeProvinciaPage } from '../pages/sedeprovincia/sedeprovincia';
import { SedeSingolaPage } from '../pages/sedesingola/sedesingola';
import { AccountPage } from '../pages/account/account';
import { PreadesioniPage } from '../pages/preadesioni/preadesioni';
import { CategoriaRicorsiPage } from '../pages/categoriaricorsi/categoriaricorsi';
import { SingoloRicorsoPage } from '../pages/singoloricorso/singoloricorso';
import { IntroduzionePage } from '../pages/introduzione/introduzione';
import { PreadesioniinviatePage } from '../pages/preadesioniinviate/preadesioniinviate';
import { ContenziosoPage } from '../pages/contenzioso/contenzioso';
import { ModalpreinviatePage } from '../pages/modalpreinviate/modalpreinviate';



import { SediProvider } from '../providers/sedi/sedi';
import { NewsProvider } from '../providers/news/news';

import {NgPipesModule} from 'ngx-pipes';
import { GroupByPipe } from '../pipes/group-by/group-by';
import { SearchPipe } from '../pipes/search/search';
import { SortPipe } from '../pipes/sort/sort';
import { UniquePipe } from '../pipes/unique/unique';
import { AuthProvider } from '../providers/auth/auth';
import { RicorsiProvider } from '../providers/ricorsi/ricorsi';



@NgModule({
  declarations: [
    MyApp,
    GroupByPipe,
    SearchPipe,
    UniquePipe,
    SortPipe,
    HomePage,
    ContattiPage,
    ContattisegreteriaPage,
    SingolanewsPage,
    ContattiamministrazionePage,
    LoginPage,
    RegisterPage,
    RicorsiPage,
    SedinazionaliPage,
    SedeProvinciaPage,
    SedeSingolaPage,
    AccountPage,
    PreadesioniPage,
    CategoriaRicorsiPage,
    SingoloRicorsoPage,
    WelcomePage,
    IntroduzionePage,
    PreadesioniinviatePage,
    ContenziosoPage,
    NotifichePage,
    SingolanotificaPage,
    ModalpreinviatePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    NgPipesModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp)

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ContattiPage,
    ContattisegreteriaPage,
    SingolanewsPage,
    ContattiamministrazionePage,
    LoginPage,
    RegisterPage,
    RicorsiPage,
    SedinazionaliPage,
    SedeProvinciaPage,
    SingolanotificaPage,
    NotifichePage,
    SedeSingolaPage,
    AccountPage,
    PreadesioniPage,
    CategoriaRicorsiPage,
    SingoloRicorsoPage,
    WelcomePage,
    IntroduzionePage,
    PreadesioniinviatePage,
    ContenziosoPage,
    ModalpreinviatePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    NewsProvider,
    SediProvider,
    AuthProvider,
    RicorsiProvider
  ]
})
export class AppModule {}
