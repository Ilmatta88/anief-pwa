import { Component, ViewChild } from '@angular/core';
import { Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { ContattiPage } from '../pages/contatti/contatti';
import { LoginPage } from '../pages/login/login';
import { WelcomePage } from '../pages/welcome/welcome';
import { RicorsiPage } from '../pages/ricorsi/ricorsi';
import {IntroduzionePage} from '../pages/introduzione/introduzione';
import {PreadesioniinviatePage } from '../pages/preadesioniinviate/preadesioniinviate';
import { PreadesioniPage } from '../pages/preadesioni/preadesioni';
import { ContenziosoPage } from '../pages/contenzioso/contenzioso';
import { NotifichePage } from '../pages/notifiche/notifiche';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

    rootPage:any = WelcomePage;
    pages: Array<{title: string, component: any}>;
    pages2: any;
    submenus: Array<{title: string, component: any}>;
mainmenus: Array<{title: string, component: any}>;
shownGroup = null;

    constructor(
      public statusBar: StatusBar,
      public splashScreen: SplashScreen,) {






      this.pages = [
        { title: 'Home', component: WelcomePage },
        {title: 'News', component: HomePage},
        { title: 'Contatti', component: ContattiPage },
        { title: 'Ricorsi', component: RicorsiPage },
        { title: 'Login', component: LoginPage }

      ];

      this.pages2 = {
      homePage: HomePage,
      contattiPage: ContattiPage,
      loginPage: LoginPage,
      welcomePage: WelcomePage,
      ricorsiPage: RicorsiPage,

    }

     this.submenus = [{ title: 'Preadesioni Inviate', component: PreadesioniinviatePage },
    {title: 'Preadesioni da Completare', component: PreadesioniPage},
    { title: 'Contenzioso', component: ContenziosoPage },
    { title: 'Notifiche', component: NotifichePage }
  ];

    }

    toggleGroup(group) {
    if (this.isGroupShown(group)) {
        this.shownGroup = null;
    } else {
        this.shownGroup = group;
    }
  }
  isGroupShown(group) {
      return this.shownGroup === group;
  }

    openPage(page) {
      this.nav.setRoot(page.component);
    }

    getKeys(obj){
    return Object.keys(obj)
  }
}
